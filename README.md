# Robotics Project

##Guitar tuner

First task, Aleksandra
First task, Liga Britala
First task, Karina
First task, Kristen

Overview 
The aim of this project is to build a robot that would help to tune a guitar. 
A guitar has six strings, which produce sounds of specific frequencies. 
When a guitar is tuned properly the frequency that each string produces is as follows:
E or the thinnest string 329.63 Hz
B 246.94 Hz
G 196.00 Hz
D 146.83 Hz
A 110.00 Hz
E or the thickest string 82.41 Hz.
Being out of tune simply means that a string produces sound that is of lower or higher frequency than shown above. 
By turning the peg in a direction that tightens the string one can increase the pitch (frequency) and vice versa. 
This project can be divided into two main tasks. Input and output. 
Input part consists of recording the sound using a microphone, detecting the frequency of the sound produced and 
analysing if and by how much it differs from the supposed frequency value of the respective string.

Output part of the project consists of one servo motor with extra pins on the arms of the servo. The peg of the string 
will fit between those pins. Based on the input data servo will either rotate right or left which will in turn tighten 
or loosen the peg while the body of the servo is held in hand.
LED lights will help with visualizing the process. 6 red LED lights, each representing one string, are connected to 
Arduino via breadboard and will signal if the frequency of the sound is out of norm. 1 green LED will light up if 
the desired frequency is reached. The button will switch the detection between lights/strings. 

Main schedule 
Week 10 – 11 Discussing the project with lab instructors, looking up tutorials and coming up with general plan.
Week 11 – 12 Beginning to work on code/parts separately, each pair by themselves. Making demo video.
Week 13 – 14 Continue working on code/parts, combining input and output. Presenting video. Making poster.
Week 15 – 16 Presenting poster, working on final solution. Presenting final project. 


Schedule for pair A, Liga and Aleksandra
Pair A will be working on output part of the project and documentation, including video and poster making. 
Week 10 Project plan, online research and consulting lab instructors. Putting algorithm on paper. 
Week 11 Testing of servo on guitar, working on code and hardware
Week 12 Demo video 
Week 13 Combining two tasks together and testing solution 
Week 14 Debugging, making poster. 
Week 15 Last minute touch-ups. 

Schedule for pair B, Karina and Kristen
Pair B will be working on input part of the project aka sound processing.
Week 10 Project plan, online research and consulting lab instructors. Putting algorithm on paper
Week 11 Testing of microphone, working on code and software
Week 12 Testing of code
Week 13 Combining two tasks together and testing solution
Week 14 Debugging
Week 15 Last minute touch-ups

Components 
Item		       Quantity		Needed from the lab
		          				   Yes	        No
Guitar	     		    1						x
Microphone*   			1	        x	
Breadboard				1       	x	
Continuous Servo motor	1	        x	
Arduino 				1       	x	
LED (red)				6	        x	
LED (green)	  			1	 		x	
Jumper cables**			n			x	
Battery**				n			x	
Resistor**				n			x	
* Preferably Arduino microphone with amplifier	
** Yet to be specified

