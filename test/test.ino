// Sweep
// by BARRAGAN <http://barraganstudio.com> 
// This example code is in the public domain.


#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
 
int pos = 0;    // variable to store the servo position 
 
void setup() 
{ 
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object 
}                     //tegemist siis D9-ga
 
 
void loop() 
{ 
    myservo.write(40);              // Ütlen servole, et ta läheks positsiooni 40
    delay(2000);                       // ootab sekundi, et edasi minna 

    myservo.write(90);               //siis ütlen, et liiguks edasi eelmisest positsioonist 90ni
    delay(2000);      //millisecundid

    myservo.write(130);
    delay(2000);

    myservo.write(180);
    delay(1000);
    
    //myservo.write(0);   see on näiteks, et kui eelmises kohas jõudis ta 180, siis siin ta kohe kiiresti läheb 0 pos ja
    //delay(1000);        siis järgmises tsüklis ta läheb uuesti pos 180 ja liigub siis aeglaselt tagasi nulli 
  
  for(pos = 180; pos>=1; pos-=1)     // goes from 180 degrees to 0 degrees //ta teab kuna seisma jääda siis kui pos>=1 ei vatsa enam 
                                     //tõele, ehk on näiteks 0, siis ta jääb seisma ja void loop algab otsast peale
  {                                
    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  }
} 
