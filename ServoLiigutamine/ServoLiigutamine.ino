#include <Servo.h>

Servo myservo;

int servoKesk = 90;
int currentState = 90;
int pinnid[] = {2, 3, 4, 6, 7, 8};

int keel = 0;
int sagedus = 0;

void setup() {
  
  Serial.begin(9600);

  myservo.attach(9);

  pinMode(pinnid[0], OUTPUT);
  pinMode(pinnid[1], OUTPUT);
  pinMode(pinnid[2], OUTPUT);
  pinMode(pinnid[3], OUTPUT);
  pinMode(pinnid[4], OUTPUT);
  pinMode(pinnid[5], OUTPUT);

  pinMode(12, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  

}

void loop() {

  if (digitalRead(12) == LOW) {
    keel += 1;

    if (keel == 6) {
      keel = 0;
    }

  }


  for (int i = 0; i < 6; i += 1) {
    digitalWrite(pinnid[i], LOW);
  }

  digitalWrite(pinnid[keel], HIGH);

  // put your main code here, to run repeatedly:

  sagedus = Serial.parseInt();  //andmed = "0 345" - andmed on sellisel kujul

  if (digitalRead(5) == LOW){
    myservo.write(90);
  }
  
  Serial.println(sagedus);
  // keel - string
  // sagedus - frequency
  // vahe - difference

  // higher E
  if (keel == 0 and sagedus > 310 and sagedus < 350) {
    int vahe = sagedus - 330;
    if (vahe > 0) {
      if (currentState < 176) {
        myservo.write(currentState + 4);
        currentState += 1;
      }
      Serial.println(currentState);
      Serial.println(sagedus);
      Serial.println("                             Suurem kui null");
    }
    else if (vahe < 0) {
      if (currentState > 4) {
        myservo.write(currentState - 4);
        currentState -= 1;
      }
      Serial.println(currentState);
      Serial.println(sagedus);
      Serial.println("             Vaksem kui null");
    } else {                              // if(frequency == 330)
      Serial.println("Haalestatud");
    }
  }

  // B
  if (keel == 1 and sagedus > 227 and sagedus < 267) {
    int vahe = sagedus - 247;
    if (vahe > 0) {
      if (currentState < 176) {
        myservo.write(currentState + 4);
        currentState += 1;
      }
      Serial.println(currentState);
      Serial.println(sagedus);
      Serial.println("                             Suurem kui null");
    }
    else if (vahe < 0) {
      if (currentState > 4) {
        myservo.write(currentState - 4);
        currentState -= 1;
      }
      Serial.println(currentState);
      Serial.println(sagedus);
      Serial.println("             Vaksem kui null");
    } else {                              // if(frequency == 330)
      Serial.println("Haalestatud");
    }
  }

  // G
  if (keel == 2 and sagedus > 176 and sagedus < 216) {
    int vahe = sagedus - 196;
    if (vahe > 0) {
      if (currentState < 176) {
        myservo.write(currentState + 4);
        currentState += 1;
      }
      Serial.println(currentState);
      Serial.println(sagedus);
      Serial.println("                             Suurem kui null");
    }
    else if (vahe < 0) {
      if (currentState > 4) {
        myservo.write(currentState - 4);
        currentState -= 1;
      }
      Serial.println(currentState);
      Serial.println(sagedus);
      Serial.println("             Vaksem kui null");
    } else {                              // if(frequency == 330)
      Serial.println("Haalestatud");
    }
  }

  // D
  if (keel == 3 and sagedus > 127 and sagedus < 167) {
    int vahe = sagedus - 147;
    if (vahe > 0) {
      if (currentState < 176) {
        myservo.write(currentState + 4);
        currentState += 1;
      }
      Serial.println(currentState);
      Serial.println(sagedus);
      Serial.println("                             Suurem kui null");
    }
    else if (vahe < 0) {
      if (currentState > 4) {
        myservo.write(currentState - 4);
        currentState -= 1;
      }
      Serial.println(currentState);
      Serial.println(sagedus);
      Serial.println("             Vaksem kui null");
    } else {                              // if(frequency == 330)
      Serial.println("Haalestatud");
    }
  }

  // A
  if (keel == 4 and sagedus > 90 and sagedus < 130) {
    int vahe = sagedus - 110;
    if (vahe > 1) {
      if (currentState < 176) {
        myservo.write(currentState + 4);
        currentState += 1;
      }
      Serial.println(currentState);
      Serial.println(sagedus);
      Serial.println("                             Suurem kui null");
    }
    else if (vahe < -1) {
      if (currentState > 4) {
        myservo.write(currentState - 4);
        currentState -= 1;
      }
      Serial.println(currentState);
      Serial.println(sagedus);
      Serial.println("             Vaksem kui null");
    } else {                              // if(frequency == 330)
      Serial.println("Haalestatud");
    }
  }

  // Lower E
  if (keel == 5 and sagedus > 72 and sagedus < 92) {
    int vahe = sagedus - 82;
    if (vahe > 1) {
      if (currentState < 176) {
        myservo.write(currentState + 4);
        currentState += 1;
      }
      Serial.println(currentState);
      Serial.println(sagedus);
      Serial.println("                             Suurem kui null");
    }
    else if (vahe < -1) {
      if (currentState > 4) {
        myservo.write(currentState - 4);
        currentState -= 1;
      }
      Serial.println(currentState);
      Serial.println(sagedus);
      Serial.println("             Vaksem kui null");
    } else {                              // if(frequency == 330)
      Serial.println("Haalestatud");
    }
  }

  //peale tunimise loppmist keera servo tagasi kesk punkt
}
